use ipnet::Ipv4Net;
use std::net::Ipv4Addr;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub vpns: Vec<VpnConfig>,
    pub users: Vec<UserConfig>,
    pub global: GlobalConfig,
    pub server: ServerConfig,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ServerConfig {
    pub networks: Ipv4Net, // gets split into /24's
    pub tinc_low_port: u16,
    pub tinc_high_port: u16,
    pub tinc_address: Ipv4Addr,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct GlobalConfig {
    pub local_routes: Vec<Ipv4Net>,
    pub default_dns: Vec<Ipv4Addr>,
    pub local_dns: Vec<LocalDnsConfig>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LocalDnsConfig {
    pub domain: String,
    pub server: Ipv4Addr,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserConfig {
    pub username: String,
    pub bcrypt: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct VpnConfig {
    pub id: String,
    pub name: String,
    #[serde(flatten)]
    pub vpn: EndpointConfig,
    pub routes: Vec<Ipv4Net>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserPass {
    pub username: String,
    pub password: String,
}

fn constant_false() -> bool {
    false
}

fn no_routes() -> Vec<Ipv4Net> {
    vec![]
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum OpenConnectProtocol {
    AnyConnect,
    /// Juniper Network Connect/Junos Pulse
    NC,
    /// PAN GlobalProtect
    GP,
    /// Modern pulse
    Pulse,
}

fn anyconnect() -> OpenConnectProtocol {
    OpenConnectProtocol::AnyConnect
}

#[derive(Serialize, Deserialize, Debug)]
pub enum EndpointConfig {
    #[serde(rename = "openconnect")]
    OpenConnect {
        username: String,
        password: String,
        endpoint: String,
        servercert: Option<String>,
        authgroup: Option<String>,
        #[serde(rename = "manual-routes", default = "no_routes")]
        manual_routes: Vec<Ipv4Net>,
        #[serde(default = "anyconnect")]
        protocol: OpenConnectProtocol,
    },
    #[serde(rename = "openvpn")]
    OpenVPN {
        #[serde(rename = "auth-user-pass")]
        auth_user_pass: Option<UserPass>,
        config: String,
    },
    #[serde(rename = "openfortivpn")]
    OpenFortiVPN {
        username: String,
        password: String,
        host: String,
        port: u16,
        #[serde(rename = "trusted-cert")]
        trusted_cert: String,
    },
    #[serde(rename = "pptp")]
    PPTP {
        username: String,
        password: String,
        host: String,
        #[serde(rename = "manual-routes")]
        manual_routes: Option<Vec<Ipv4Net>>,
        #[serde(rename = "require-mppe-128", default = "constant_false")]
        require_mppe_128: bool,
        #[serde(rename = "refuse-pap", default = "constant_false")]
        refuse_pap: bool,
        #[serde(rename = "refuse-eap", default = "constant_false")]
        refuse_eap: bool,
        #[serde(rename = "refuse-chap", default = "constant_false")]
        refuse_chap: bool,
        #[serde(rename = "refuse-mschap", default = "constant_false")]
        refuse_mschap: bool,
        #[serde(rename = "require-mppe", default = "constant_false")]
        require_mppe: bool,
    },
    #[serde(rename = "netExtender")]
    NetExtender {
        host: String,
        username: String,
        password: String,
        domain: String,
        cert: Option<String>,
    },
}

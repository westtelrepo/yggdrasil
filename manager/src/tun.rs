use std::io;
use std::process::Command;

pub struct Tun {
    name: String,
}

impl Tun {
    pub fn new(name: &str) -> io::Result<Tun> {
        let result = Command::new("ip")
            .args(&["tuntap", "add", "dev"])
            .arg(name)
            .args(&["mode", "tun"])
            .output()?;

        assert!(result.status.success());

        Ok(Tun {
            name: name.to_string(),
        })
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Drop for Tun {
    fn drop(&mut self) {
        Command::new("ip")
            .args(&["tuntap", "del", "dev"])
            .arg(&self.name)
            .args(&["mode", "tun"])
            .output()
            .expect("could not delete tun device");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let name = "tun1874";
        {
            let t = Tun::new(name);

            let output = Command::new("ip")
                .args(&["tuntap", "show"])
                .output()
                .expect("could not list tuntap devices");
            let output = String::from_utf8(output.stdout).unwrap();

            assert!(output.find(name).is_some());
        }
        let output = Command::new("ip")
            .args(&["tuntap", "show"])
            .output()
            .expect("could not list tuntap devices");
        let output = String::from_utf8(output.stdout).unwrap();

        assert!(!output.find(name).is_some());
    }

}

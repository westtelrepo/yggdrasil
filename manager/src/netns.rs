use std::io;
use std::process::Command;

pub struct NetNS {
    name: String,
}

impl NetNS {
    pub fn new(name: &str) -> io::Result<NetNS> {
        let result = Command::new("ip")
            .args(&["netns", "add"])
            .arg(name)
            .output()?;

        assert!(result.status.success());

        let ret = NetNS {
            name: name.to_string(),
        };

        let result = Command::new("ip")
            .args(&["netns", "exec"])
            .arg(&ret.name)
            .args(&["ip", "link", "set", "dev", "lo", "up"])
            .output()?;

        assert!(result.status.success());

        Ok(ret)
    }
}

impl Drop for NetNS {
    fn drop(&mut self) {
        Command::new("ip")
            .args(&["netns", "delete"])
            .arg(&self.name)
            .output()
            .expect("could not delete netns");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let name = "netns1849";
        {
            let n = NetNS::new(name);

            let output = Command::new("ip")
                .args(&["netns", "list"])
                .output()
                .expect("could not list netns");
            let output = String::from_utf8(output.stdout).unwrap();

            assert!(output.find(name).is_some());
        }

        let output = Command::new("ip")
            .args(&["netns", "list"])
            .output()
            .expect("could not list netns");
        let output = String::from_utf8(output.stdout).unwrap();

        assert!(!output.find(name).is_some());
    }
}

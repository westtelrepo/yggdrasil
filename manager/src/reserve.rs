use std::sync::{Arc, Mutex};

pub struct ReserveSet<T> {
    set: Arc<Mutex<Vec<T>>>,
}

impl<T> ReserveSet<T>
where
    T: Copy,
{
    pub fn new<I>(iter: I) -> ReserveSet<T>
    where
        I: IntoIterator<Item = T>,
    {
        ReserveSet {
            set: Arc::new(Mutex::new(iter.into_iter().collect())),
        }
    }

    pub fn reserve(&self) -> Option<Item<T>> {
        let mut set = self.set.lock().unwrap();

        let item = set.pop();

        if let Some(item) = item {
            Some(Item {
                set: Arc::clone(&self.set),
                item,
            })
        } else {
            None
        }
    }
}

pub struct Item<T>
where
    T: Copy,
{
    set: Arc<Mutex<Vec<T>>>,
    item: T,
}

impl<T> Item<T>
where
    T: Copy,
{
    pub fn item(&self) -> T {
        self.item
    }
}

impl<T> Drop for Item<T>
where
    T: Copy,
{
    fn drop(&mut self) {
        self.set.lock().unwrap().push(self.item);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let set = ReserveSet::new(vec![1, 2]);

        let item1 = set.reserve().unwrap();

        assert!(item1.item() == 1 || item1.item() == 2);

        let item2 = set.reserve().unwrap();

        assert!(item2.item() == 1 || item2.item() == 2);
        assert_ne!(item1.item(), item2.item());
    }

    #[test]
    fn drop_works() {
        let set = ReserveSet::new(vec![1, 2]);

        let item1 = set.reserve().unwrap();
        assert!(item1.item() == 1 || item1.item() == 2);

        let item2_item;

        {
            let item2 = set.reserve().unwrap();
            assert!(item2.item() == 1 || item2.item() == 2);
            assert_ne!(item1.item(), item2.item());
            item2_item = item2.item();
        }

        let item3 = set.reserve().unwrap();
        assert!(item3.item() == 1 || item3.item() == 2);
        assert_eq!(item2_item, item3.item());
        assert_ne!(item1.item(), item3.item());
    }
}

#![forbid(unsafe_code)]
extern crate nix;
#[macro_use]
extern crate rouille;
extern crate serde;
extern crate tempfile;
extern crate uuid;
#[macro_use]
extern crate serde_derive;
extern crate bcrypt;
extern crate ipnet;
extern crate jsonwebtoken;
extern crate openssl;
extern crate serde_yaml;

mod config;
mod nspawn;
mod reserve;
mod vpns;

use bcrypt::{hash, verify, DEFAULT_COST};
use config::Config;
use ipnet::Ipv4Net;
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use openssl::rsa::Rsa;
use reserve::ReserveSet;
use rouille::Response;
use std::collections::HashMap;
use std::env::VarError;
use std::fs;
use std::net::{IpAddr, SocketAddr};
use std::process::Command;
use std::sync::{Arc, Condvar, Mutex};
use std::time::SystemTime;
use uuid::Uuid;
use vpns::{
    create_net_extender, create_openconnect, create_openfortivpn, create_openvpn, create_pptp,
    create_tinc,
};

fn handle_root(_request: &rouille::Request, auth_cookie: &Option<JwtLogin>) -> Response {
    match auth_cookie {
        Some(_) => {
            let html = r#"<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title>YGGDRASIL</title>
</head>
<body>
<div id="root"></div>
<noscript>You need Javascript!</noscript>
<script src="/indexjs"></script>
</body>
</html>
"#;

            Response::html(html)
        }
        None => Response::redirect_303("/login"),
    }
}

struct Vpn {
    #[allow(dead_code)]
    nspawn: nspawn::NSpawn,
    #[allow(dead_code)]
    network: reserve::Item<Ipv4Net>,
    #[allow(dead_code)]
    port: reserve::Item<u16>,
    #[allow(dead_code)]
    vpn: nspawn::NSpawn,
    #[allow(dead_code)]
    veth_network: reserve::Item<Ipv4Net>,
    vpn_id: String,
    for_user: String,
    // informational purposes only
    routes: Vec<Ipv4Net>,
    client_hostname: Option<String>,
}

fn handle_connection(
    request: &rouille::Request,
    config: &Config,
    ports: &ReserveSet<u16>,
    subnets: &ReserveSet<Ipv4Net>,
    tincs: &Arc<Mutex<HashMap<uuid::Uuid, Vpn>>>,
    jwt_public_key: &DecodingKey,
) -> Response {
    println!("creating connection");

    let auth;

    if let Some(authorization) = request.header("Authorization") {
        let v: Vec<&str> = authorization.split(char::is_whitespace).collect();
        assert!(v.len() == 2);
        assert_eq!(v[0], "Bearer");

        let mut validation = Validation::new(Algorithm::RS256);
        validation.validate_nbf = true;
        validation.validate_exp = true;
        validation.set_audience(&["jwt-connection-bootstrap"]);

        let claims = decode::<JwtBootstrapClaims>(&v[1], &jwt_public_key, &validation).unwrap();

        auth = claims.claims;
    } else {
        println!("unauthorized");
        return Response::text("Unauthorized!").with_status_code(403);
    }

    #[derive(Deserialize)]
    #[serde(deny_unknown_fields)]
    #[serde(tag = "type")]
    enum VpnType {
        Tinc {
            public_key: String,
            // tinc "name" is always "user"
        },
    }

    #[derive(Deserialize)]
    #[serde(deny_unknown_fields)]
    struct Create {
        site: String,
        vpn: VpnType,
        hostname: Option<String>,
    }

    let create: Create = try_or_400!(rouille::input::json_input(request));

    assert_eq!(create.site, auth.sub);

    let vpn_config = config.vpns.iter().find(|x| x.id == create.site).unwrap();

    let VpnType::Tinc { public_key } = create.vpn;

    let user_public_key = Rsa::public_key_from_pem_pkcs1(public_key.as_bytes()).unwrap();

    let id = Uuid::new_v4();

    let network = subnets.reserve().unwrap();
    let port = ports.reserve().unwrap();
    let veth_network = subnets.reserve().unwrap();

    let veth_ip_1 = veth_network.item().hosts().nth(0).unwrap();
    let veth_ip_2 = veth_network.item().hosts().nth(1).unwrap();

    let id_string = id.to_simple().to_string();
    let veth_1_name = format!("va-{}", &id_string[..12]);
    let veth_2_name = format!("vb-{}", &id_string[..12]);

    // 15 is the maximum length for an interface name
    assert!(veth_1_name.len() <= 15);
    assert!(veth_2_name.len() <= 15);

    let key = Rsa::generate(3072).unwrap();
    let public_pem = String::from_utf8(key.public_key_to_pem_pkcs1().unwrap()).unwrap();

    Command::new("ip")
        .args(&[
            "link",
            "add",
            "name",
            &veth_1_name,
            "type",
            "veth",
            "peer",
            "name",
            &veth_2_name,
        ])
        .output()
        .unwrap();

    let port_number = port.item();

    let nspawn = create_tinc(
        veth_ip_1,
        veth_ip_2,
        veth_network.item(),
        network.item(),
        port_number,
        user_public_key,
        key,
        &veth_1_name,
        &vpn_config.routes,
        &config.global,
    );

    let vpn = match vpn_config.vpn {
        config::EndpointConfig::OpenConnect {
            ref endpoint,
            ref username,
            ref servercert,
            ref password,
            ref authgroup,
            ref manual_routes,
            protocol,
        } => create_openconnect(
            veth_ip_1,
            veth_ip_2,
            veth_network.item().prefix_len(),
            network.item(),
            &veth_2_name,
            &endpoint,
            &username,
            &servercert,
            &password,
            &authgroup,
            &manual_routes,
            protocol,
        ),
        config::EndpointConfig::OpenVPN {
            ref auth_user_pass,
            ref config,
        } => create_openvpn(
            veth_ip_1,
            veth_ip_2,
            veth_network.item().prefix_len(),
            network.item(),
            &veth_2_name,
            auth_user_pass,
            &config,
        ),
        config::EndpointConfig::OpenFortiVPN {
            ref username,
            ref password,
            ref trusted_cert,
            ref host,
            port,
        } => create_openfortivpn(
            veth_ip_1,
            veth_ip_2,
            veth_network.item().prefix_len(),
            network.item(),
            &veth_2_name,
            &host,
            port,
            &username,
            &password,
            &trusted_cert,
        ),
        config::EndpointConfig::PPTP {
            ref username,
            ref password,
            ref host,
            ref manual_routes,
            require_mppe_128,
            refuse_pap,
            refuse_eap,
            refuse_chap,
            refuse_mschap,
            require_mppe,
        } => {
            let empty_vec = vec![];
            let manual_routes = match manual_routes {
                Some(routes) => routes,
                None => &empty_vec,
            };
            create_pptp(
                veth_ip_1,
                veth_ip_2,
                veth_network.item().prefix_len(),
                network.item(),
                &veth_2_name,
                &host,
                &username,
                &password,
                &manual_routes,
                require_mppe_128,
                refuse_pap,
                refuse_eap,
                refuse_chap,
                refuse_mschap,
                require_mppe,
            )
        }
        config::EndpointConfig::NetExtender {
            ref host,
            ref username,
            ref password,
            ref domain,
            ref cert,
        } => create_net_extender(
            veth_ip_1,
            veth_ip_2,
            veth_network.item().prefix_len(),
            network.item(),
            &veth_2_name,
            &host,
            &username,
            &password,
            &domain,
            &cert,
        ),
    };

    let vpn = Vpn {
        nspawn,
        network,
        port,
        vpn,
        veth_network,
        vpn_id: create.site,
        for_user: auth.for_user,
        routes: vpn_config.routes.clone(),
        client_hostname: create.hostname,
    };

    tincs.lock().unwrap().insert(id, vpn);

    #[derive(Serialize)]
    struct HostConfig {
        address: std::net::SocketAddr,
        public_key: String,
        name: String,
    }

    #[derive(Serialize)]
    struct Resp {
        id: String,
        tinc_hosts: Vec<HostConfig>,
    }

    let tinc_hosts = vec![HostConfig {
        address: SocketAddr::new(IpAddr::V4(config.server.tinc_address), port_number),
        public_key: public_pem.clone(),
        name: "host".to_string(),
    }];

    let resp = Resp {
        id: id.to_simple().to_string(),
        tinc_hosts,
    };

    Response::json(&resp)
}

#[derive(Debug, Serialize, Deserialize)]
struct JwtBootstrapClaims {
    sub: String, // vpn id
    exp: u64,
    iat: u64,
    nbf: u64,
    for_user: String, // user id
    aud: String,      // jwt-connection-bootstrap
}

#[derive(Debug, Serialize, Deserialize)]
struct JwtLogin {
    sub: String, // name
    exp: u64,
    iat: u64,
    nbf: u64,
    aud: String, // jwt-session
}

fn handle_new(
    _request: &rouille::Request,
    id: &str,
    jwt_private_key: &EncodingKey,
    auth_cookie: &Option<JwtLogin>,
) -> Response {
    match auth_cookie {
        Some(auth_cookie) => {
            let now = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();

            let claims = JwtBootstrapClaims {
                sub: id.to_owned(),
                exp: now.as_secs() + 10 * 60,
                nbf: now.as_secs() - 60,
                iat: now.as_secs(),
                for_user: auth_cookie.sub.clone(),
                aud: "jwt-connection-bootstrap".to_owned(),
            };

            let mut header = Header::default();
            header.alg = Algorithm::RS256;

            let token = encode(&header, &claims, &jwt_private_key).unwrap();

            Response::text(token)
        }
        None => Response::redirect_303("/login"),
    }
}

// TODO: don't use String error
fn create_login_cookie(username: &str, jwt_private_key: &EncodingKey) -> Result<String, String> {
    let now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .map_err(|err| err.to_string())?;

    let cookie = JwtLogin {
        aud: "jwt-session".to_owned(),
        exp: now.as_secs() + 10 * 60,
        iat: now.as_secs(),
        nbf: now.as_secs() - 10,
        sub: username.to_owned(),
    };

    return encode(&Header::new(Algorithm::RS256), &cookie, &jwt_private_key)
        .map_err(|err| err.to_string());
}

fn version() -> String {
    format!(
        "1.0.0-alpha.5+{}{}",
        env!("GIT_SHORT_HASH"),
        if env!("GIT_DIRTY") == "true" {
            "-dirty"
        } else {
            ""
        }
    )
}

fn main() {
    println!("version: {}", version());

    let config_file = match std::env::var("YGG_CONFIG") {
        Ok(val) => val,
        Err(e) => match e {
            VarError::NotPresent => "/vagrant/manager/config.yml".to_owned(),
            _ => panic!("YGG_CONFIG set incorrectly"),
        },
    };
    let config_file = fs::File::open(config_file).unwrap();
    let config: Config = serde_yaml::from_reader(config_file).unwrap();

    // println!("{}", serde_yaml::to_string(&config).unwrap());

    let ports: ReserveSet<u16> =
        ReserveSet::new(config.server.tinc_low_port..config.server.tinc_high_port);
    let subnets = ReserveSet::new(config.server.networks.subnets(24).unwrap());

    let tincs = Arc::new(Mutex::new(HashMap::new()));
    // notify if a connection has been deleted
    let cond_var = Condvar::new();

    let (jwt_private_key, jwt_public_key) = {
        let jwt_key = Rsa::generate(3072).unwrap();
        (
            EncodingKey::from_rsa_pem(&jwt_key.private_key_to_pem().unwrap())
                .expect("could not import generated RSA private key")
                .clone(),
            // NOTE: yes this leaks memory, currently it leaks a constant amount of memory
            // see <https://github.com/Keats/jsonwebtoken/issues/120>
            // from_rsa_pem requires a static lifetime (... weirdly)
            DecodingKey::from_rsa_pem(Box::leak(Box::new(jwt_key.public_key_to_pem().unwrap())))
                .expect("could not import generated RSA public key")
                .clone(),
        )
    };

    rouille::start_server("127.0.0.1:8080", move |request| {
        let auth_cookie = rouille::input::cookies(&request).find(|&(n, _)| n == "__Host-jwt-login");
        let mut auth_cookie = match auth_cookie {
            Some((_, val)) => {
                let mut validation = Validation::new(Algorithm::RS256);
                validation.validate_exp = true;
                validation.validate_nbf = true;
                validation.set_audience(&["jwt-session"]);
                let auth = decode::<JwtLogin>(&val, &jwt_public_key, &validation);

                match auth {
                    Err(_) => None,
                    Ok(val) => Some(val.claims),
                }
            }
            None => None,
        };

        let response = router!(request,
            (GET) (/) => {
                handle_root(request, &auth_cookie)
            },
            (GET) (/login) => {
                Response::html(r#"
<html>
<body>
<form method="post">
Username: <input type="text" name="username" /><br/>
Password: <input type="password" name="password" /><br />
<input type="submit"/>
</form>
</body>
</html>
"#)
            },
            (POST) (/login) => {
                let post = try_or_400!(post_input!(request, {
                    username: String,
                    password: String,
                }));

                let user = config.users.iter().find(|x| x.username == post.username).unwrap();

                let valid = try_or_400!(verify(&post.password, &user.bcrypt));

                if valid {
                    let cookie = create_login_cookie(&post.username, &jwt_private_key).expect("could not create cookie");
                    auth_cookie = None;
                    Response::redirect_303("/").with_additional_header("Set-Cookie", format!("__Host-jwt-login={}; HttpOnly; Secure; SameSite=Lax; Path=/", cookie))
                } else {
                    Response::redirect_303("/login")
                }
            },
            // TODO: password protect this or something
            (GET) (/bcrypt) => {
                Response::html(r#"
<html>
<body>
<form method="post">
Password: <input type="password" id="password" name="password" /><br/>
<input type="submit"/>
</form>
</body>
</html>
"#)
            },
            (POST) (/bcrypt) => {
                let post = try_or_400!(post_input!(request, {
                    password: String,
                }));

                let hashed = try_or_400!(hash(&post.password, DEFAULT_COST));
                Response::text(hashed)
            },
            (GET) (/site/{id: String}) => {
                handle_new(request, &id, &jwt_private_key, &auth_cookie)
            },
            (GET) (/site) => {
                if auth_cookie.is_none() {
                    Response::text("Unauthorized!").with_status_code(403)
                } else {
                    #[derive(Serialize)]
                    struct SiteConfig {
                        id: String,
                        name: String,
                    }

                    let mut ret = Vec::new();

                    for site in &config.vpns {
                        ret.push(SiteConfig{id: site.id.clone(), name: site.name.clone()});
                    }

                    Response::json(&ret)
                }
            },
            (POST) (/connection) => {
                handle_connection(request, &config, &ports, &subnets, &tincs, &jwt_public_key)
            },
            (DELETE) (/connection/{id : String}) => {
                if auth_cookie.is_none() {
                    Response::text("Unauthorized!").with_status_code(403)
                } else {
                    println!("deleting connection");
                    let id = try_or_400!(Uuid::parse_str(&id));
                    tincs.lock().unwrap().remove(&id);
                    cond_var.notify_all();
                    Response::text(format!("delete: {}\n", id))
                }
            },
            (GET) (/connection) => {
                if auth_cookie.is_none() {
                    Response::text("Unauthorized!").with_status_code(403)
                } else {
                    #[derive(Serialize)]
                    struct ConnConfig {
                        vpn_id: String,
                        for_user: String,
                        id: String,
                        routes: Vec<Ipv4Net>,
                        hostname: Option<String>,
                    }

                    let mut ret = Vec::new();
                    for (key, value) in tincs.lock().unwrap().iter() {
                        ret.push(ConnConfig{
                            vpn_id: value.vpn_id.clone(),
                            for_user: value.for_user.clone(),
                            id: key.to_simple().to_string(),
                            routes: value.routes.clone(),
                            hostname: value.client_hostname.clone(),
                        })
                    }

                    Response::json(&ret)
                }
            },
            (GET) (/connection/{id: String}/still_exists) => {
                let uuid = Uuid::parse_str(&id).unwrap();
                let tincs = tincs.lock().unwrap();
                let connection = tincs.contains_key(&uuid);
                if connection {
                    let tincs = cond_var.wait_timeout(tincs, std::time::Duration::from_secs(55)).unwrap().0;
                    if tincs.contains_key(&uuid) {
                        Response::json(&true)
                    } else {
                        Response::json(&false)
                    }
                } else {
                    Response::json(&false)
                }
            },
            (GET) (/indexjs) => {
                if auth_cookie.is_none() {
                    Response::text("Unauthorized!").with_status_code(403)
                } else {
                    Response::from_data("application/javascript", include_str!("../../manager_web/lib/index.js"))
                }
            },
            _ => Response::text("404 Not Found\n").with_status_code(404)
        );

        // set CSP header
        let response = response.with_additional_header(
            "Content-Security-Policy",
            // script-src 'unsafe-inline' is required for React Developer Tools to work
            // emotion/reactjs seems to require style-src 'unsafe-inline'
            "block-all-mixed-content; base-uri 'self'; form-action 'self'; frame-ancestors 'none'; default-src 'none'; script-src 'self' 'unsafe-inline'; connect-src 'self'; style-src 'unsafe-inline'; img-src 'self'",
        );

        // set some other security headers
        let response = response
            // do not allow us to be framed (prevent clickjacking)
            .with_additional_header("X-Frame-Options", "deny")
            // if the browser detects an XSS attack, stop the entire page
            .with_additional_header("X-XSS-Protection", "1; mode=block")
            // don't allow this site to be loaded over plain http for a long time
            .with_additional_header("Strict-Transport-Security", "max-age=15768000")
            // we set our MIME headers correctly
            .with_additional_header("X-Content-Type-Options", "nosniff")
            // don't send referrer header
            .with_additional_header("Referrer-Policy", "no-referrer")
            // don't prefetch DNS records
            .with_additional_header("X-DNS-Prefetch-Control", "off");

        if let Some(auth_cookie) = auth_cookie {
            // renew authentication cookie
            // TODO: should we do this *every* time? It's not like it costs much
            let cookie = create_login_cookie(&auth_cookie.sub, &jwt_private_key)
                .expect("Could not create cookie");
            response.with_additional_header(
                "Set-Cookie",
                format!(
                    "__Host-jwt-login={}; HttpOnly; Secure; SameSite=Lax; Path=/",
                    cookie
                ),
            )
        } else {
            response
        }
    });
}

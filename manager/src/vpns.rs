use config::{GlobalConfig, OpenConnectProtocol, UserPass};
use ipnet::Ipv4Net;
use nspawn::{NSpawn, NSpawnOptions};
use openssl::pkey::{Private, Public};
use openssl::rsa::Rsa;
use std::net::Ipv4Addr;
use std::path::PathBuf;

fn do_iperf(vpn: &mut NSpawn) {
    let service = r#"[Unit]
Description=iperf server for debugging
After=network.target
[Service]
Type=simple
ExecStart=/usr/bin/iperf -s
DynamicUser=yes

Restart=always
RestartSec=5

ProtectSystem=strict
ProtectHome=yes
PrivateTmp=true
ProtectControlGroups=true
PrivateDevices=true

SystemCallArchitectures=native
# TODO: replace with whitelist (based on @system-service)
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io @reboot @swap

NoNewPrivileges=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictAddressFamilies=AF_INET AF_INET6
RestrictRealtime=yes
RestrictNamespaces=yes
MemoryDenyWriteExecute=yes
[Install]
WantedBy=multi-user.target
"#;

    vpn.write_file(&PathBuf::from("etc/systemd/system/iperf.service"), &service);

    vpn.exec(&["systemctl", "enable", "iperf"]).unwrap();
}

fn do_veth(
    vpn: &mut NSpawn,
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
) {
    vpn.add_interface(veth_interface);

    let veth_file_2 = format!(
        r#"[Match]
Name={}
[Network]
Address={}/{}
IPForward=true
[Route]
Gateway={}
Destination={}
"#,
        veth_interface, veth_ip_2, veth_prefixlen, veth_ip_1, vpn_network
    );

    vpn.write_file(
        &PathBuf::from(format!("etc/systemd/network/{}.network", veth_interface)),
        &veth_file_2,
    );

    vpn.exec(&["systemctl", "enable", "systemd-networkd"])
        .unwrap();
}

fn do_firewall(vpn: &mut NSpawn, veth_interface: &str, vpn_interface: &str) {
    let firewall_service = format!(
        r#"[Unit]
Description=persistent firewall
DefaultDependencies=no

Before=network-pre.target
Wants=network-pre.target

Wants=systemd-modules-load.service local-fs.target
After=systemd-modules-load.service local-fs.target

Conflicts=shutdown.target
Before=shutdown.target

[Service]
Type=oneshot
RemainAfterExit=yes
# by default don't forward anything
ExecStart=/sbin/iptables -P FORWARD DROP
# allow existing connections, responses to existing connections, etc.
ExecStart=/sbin/iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
# allow the VPN to work
ExecStart=/sbin/iptables -A FORWARD -i {} -o {} -j ACCEPT

[Install]
WantedBy=multi-user.target
"#,
        veth_interface, vpn_interface
    );

    vpn.write_file(
        &PathBuf::from("etc/systemd/system/firewall.service"),
        &firewall_service,
    );

    vpn.exec(&["systemctl", "enable", "firewall"]).unwrap();
}

pub fn create_net_extender(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
    host: &str,
    username: &str,
    password: &str,
    domain: &str,
    cert: &Option<String>,
) -> NSpawn {
    let mut vpn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/netExtender"),
        name_prefix: "nx".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    vpn.allow_ppp();

    do_veth(
        &mut vpn,
        veth_ip_1,
        veth_ip_2,
        veth_prefixlen,
        vpn_network,
        veth_interface,
    );

    do_firewall(&mut vpn, &veth_interface, "ppp0");

    let net_extender_service = r#"[Unit]
Description=netExtender client
After=network.target
[Service]
Environment=HOME=/root
ExecStart=/usr/bin/python3 /opt/netExtender_runner.py
ExecStartPre=+/sbin/iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE
ExecStopPost=+/sbin/iptables -t nat -D POSTROUTING -o ppp0 -j MASQUERADE
Restart=always
RestartSec=5
SyslogIdentifier=netExtender

CapabilityBoundingSet=CAP_NET_ADMIN
ProtectSystem=true
# ProtectHome=read-only
DeviceAllow=/dev/ppp rw
PrivateTmp=true
ProtectControlGroups=true

SystemCallArchitectures=native
# pppd needs @privileged
# TODO: replace with whitelist (based on @system-service)
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io @reboot @swap

NoNewPrivileges=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6 AF_NETLINK
RestrictRealtime=yes
RestrictNamespaces=yes
MemoryDenyWriteExecute=yes
[Install]
WantedBy=multi-user.target
"#;

    vpn.write_file(
        &PathBuf::from("etc/systemd/system/netExtender.service"),
        &net_extender_service,
    );

    let net_extender_runner = format!(
        r#"import pexpect
import sys

# don't have netExtender do its DNS weirdness that works terribly
# also don't have it try and reconnect, works terribly with self signed certs
child = pexpect.spawn('/usr/sbin/netExtender --no-reconnect --dns-only-local', encoding='utf-8')
# only log netExtender output
child.logfile_read = sys.stdout

child.expect_exact("SSL VPN Server:")
child.sendline("{}")
child.expect_exact("User:")
child.sendline("{}")
child.expect_exact("Password:")
child.sendline("{}")
child.expect_exact("Domain:")
child.sendline("{}")
{}

child.expect_exact("NetExtender connected successfully.")

child.expect_exact(["Connection interrupted.  Reconnecting...", "Exiting NetExtender client"], timeout=None)
"#,
        host,
        username,
        password,
        domain,
        match cert {
            Some(str) => format!(
                r#"child.expect_exact("Do you want to proceed? (Y:Yes, N:No, V:View Certificate)")
child.sendline("V")
child.expect("SHA1\[{}\].*Do you want to proceed\? \(Y:Yes, N:No, V:View Certificate\)")
child.sendline("Y")"#,
                str
            ),
            None => "".to_string(),
        }
    );

    vpn.write_file(
        &PathBuf::from("opt/netExtender_runner.py"),
        &net_extender_runner,
    );

    vpn.exec(&["chmod", "go-rwx", "/opt/netExtender_runner.py"])
        .unwrap();
    vpn.exec(&["systemctl", "enable", "netExtender"]).unwrap();

    vpn.boot();

    return vpn;
}

pub fn create_openvpn(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
    user_pass: &Option<UserPass>,
    config: &str,
) -> NSpawn {
    let mut vpn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/openvpn"),
        name_prefix: "ovpn".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    do_veth(
        &mut vpn,
        veth_ip_1,
        veth_ip_2,
        veth_prefixlen,
        vpn_network,
        veth_interface,
    );

    do_firewall(&mut vpn, &veth_interface, "tun0");

    vpn.write_file(&PathBuf::from("etc/openvpn/vpn.conf"), &config);

    if let Some(user_pass) = user_pass {
        vpn.write_file(
            &PathBuf::from("etc/openvpn/user-pass"),
            &format!("{}\n{}\n", &user_pass.username, &user_pass.password),
        );
    }

    vpn.mkdir(&PathBuf::from("etc/systemd/system/openvpn@vpn.service.d"));

    // it doesn't like LimitNPROC for some reason (having to do with containers, apparently LXC has the same issue)
    let override_file = match user_pass {
        Some(_) => {
            r#"[Service]
ExecStart=
ExecStart=/usr/sbin/openvpn --auth-user-pass /etc/openvpn/user-pass --daemon ovpn-%i --status /run/openvpn/%i.status 10 --cd /etc/openvpn --config /etc/openvpn/%i.conf --writepid /run/openvpn/%i.pid
LimitNPROC=infinity
ExecStartPre=/sbin/iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
ExecStopPost=/sbin/iptables -t nat -D POSTROUTING -o tun0 -j MASQUERADE
"#
        }
        None => {
            r#"[Service]
ExecStart=
ExecStart=/usr/sbin/openvpn --daemon ovpn-%i --status /run/openvpn/%i.status 10 --cd /etc/openvpn --config /etc/openvpn/%i.conf --writepid /run/openvpn/%i.pid
LimitNPROC=infinity
ExecStartPre=/sbin/iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
ExecStopPost=/sbin/iptables -t nat -D POSTROUTING -o tun0 -j MASQUERADE
"#
        }
    };

    vpn.write_file(
        &PathBuf::from("etc/systemd/system/openvpn@vpn.service.d/override.conf"),
        &override_file,
    );

    vpn.exec(&["systemctl", "enable", "openvpn@vpn"]).unwrap();

    if let Some(_) = user_pass {
        vpn.exec(&["chmod", "go-rwx", "/etc/openvpn/user-pass"])
            .unwrap();
    }
    // vpn.conf can contain private keys so might as well
    vpn.exec(&["chmod", "go-rwx", "/etc/openvpn/vpn.conf"])
        .unwrap();
    vpn.boot();

    return vpn;
}

pub fn create_pptp(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
    host: &str,
    username: &str,
    password: &str,
    manual_routes: &Vec<Ipv4Net>,
    require_mppe_128: bool,
    refuse_pap: bool,
    refuse_eap: bool,
    refuse_chap: bool,
    refuse_mschap: bool,
    require_mppe: bool,
) -> NSpawn {
    let mut vpn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/pptp"),
        name_prefix: "pptp".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    vpn.allow_ppp();

    do_veth(
        &mut vpn,
        veth_ip_1,
        veth_ip_2,
        veth_prefixlen,
        vpn_network,
        veth_interface,
    );

    do_firewall(&mut vpn, &veth_interface, "ppp0");

    let mut manual_up_string = String::new();
    manual_up_string += r#"#!/bin/bash

"#;

    for route in manual_routes {
        manual_up_string += &format!("/sbin/ip route add {} dev $1\n", route);
    }

    vpn.write_file(
        &PathBuf::from("etc/ppp/ip-up.d/manualroutes"),
        &manual_up_string,
    );

    let pptp_service = r#"[Unit]
Description=pptp client
After=network.target
[Service]
ExecStart=/usr/sbin/pppd nodetach noauth call vpn
ExecStartPre=+/sbin/iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE
ExecStopPost=+/sbin/iptables -t nat -D POSTROUTING -o ppp0 -j MASQUERADE
Restart=always
RestartSec=5

# pptp needs CAP_NET_RAW to bind the GRE socket
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_RAW
ProtectSystem=full
ProtectHome=read-only
DeviceAllow=/dev/ppp rw
PrivateTmp=true
ProtectControlGroups=true

SystemCallArchitectures=native
# pppd needs @privileged
# TODO: replace with whitelist (based on @system-service)
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io @reboot @swap

NoNewPrivileges=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6 AF_NETLINK AF_PACKET
RestrictRealtime=yes
RestrictNamespaces=yes
MemoryDenyWriteExecute=yes
[Install]
WantedBy=multi-user.target
"#;

    vpn.write_file(
        &PathBuf::from("etc/systemd/system/pptp.service"),
        &pptp_service,
    );

    let mut config = format!(
        r#"pty "pptp {} --nolaunchpppd"
name {}
password {}
remotename PPTP
"#,
        &host, &username, &password
    );

    if require_mppe_128 {
        config += "require-mppe-128\n";
    }

    if refuse_pap {
        config += "refuse-pap\n";
    }

    if refuse_eap {
        config += "refuse-eap\n";
    }

    if refuse_chap {
        config += "refuse-chap\n";
    }

    if refuse_mschap {
        config += "refuse-mschap\n";
    }

    if require_mppe {
        config += "require-mppe\n";
    }

    vpn.write_file(&PathBuf::from("etc/ppp/peers/vpn"), &config);

    vpn.exec(&["systemctl", "enable", "pptp"]).unwrap();
    vpn.exec(&["chmod", "a+x", "/etc/ppp/ip-up.d/manualroutes"])
        .unwrap();
    vpn.boot();

    return vpn;
}

pub fn create_openfortivpn(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
    host: &str,
    port: u16,
    username: &str,
    password: &str,
    trusted_cert: &str,
) -> NSpawn {
    let mut vpn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/openfortivpn"),
        name_prefix: "ofv".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    vpn.allow_ppp();

    do_veth(
        &mut vpn,
        veth_ip_1,
        veth_ip_2,
        veth_prefixlen,
        vpn_network,
        veth_interface,
    );

    do_firewall(&mut vpn, &veth_interface, "ppp0");

    let openfortivpn_service = r#"[Unit]
Description=OpenFortiVPN client
After=network.target
[Service]
ExecStart=/usr/bin/openfortivpn
ExecStartPre=+/sbin/iptables -t nat -A POSTROUTING -o ppp0 -j MASQUERADE
ExecStopPost=+/sbin/iptables -t nat -D POSTROUTING -o ppp0 -j MASQUERADE
Restart=always
RestartSec=5

# TODO: openfortivpn is blocked from writing to /etc/resolv.conf, but we don't want it to anyway
CapabilityBoundingSet=CAP_NET_ADMIN
ProtectSystem=full
ProtectHome=read-only
DeviceAllow=/dev/ppp rw
PrivateTmp=true
ProtectControlGroups=true

SystemCallArchitectures=native
# pppd needs @privileged
# TODO: replace with whitelist (based on @system-service)
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io @reboot @swap

NoNewPrivileges=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6 AF_NETLINK
RestrictRealtime=yes
RestrictNamespaces=yes
MemoryDenyWriteExecute=yes
[Install]
WantedBy=multi-user.target
"#;

    vpn.write_file(
        &PathBuf::from("etc/systemd/system/openfortivpn.service"),
        &openfortivpn_service,
    );

    let config = format!(
        r#"host = {}
port = {}
username = {}
password = {}
trusted-cert = {}
set-dns = 0
"#,
        host, port, username, password, trusted_cert
    );

    vpn.write_file(&PathBuf::from("etc/openfortivpn/config"), &config);

    vpn.exec(&["systemctl", "enable", "openfortivpn"]).unwrap();
    vpn.exec(&["chmod", "go-rwx", "/etc/openfortivpn/config"])
        .unwrap();
    vpn.boot();

    return vpn;
}

pub fn create_openconnect(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_prefixlen: u8,
    vpn_network: Ipv4Net,
    veth_interface: &str,
    endpoint: &str,
    username: &str,
    servercert: &Option<String>,
    password: &str,
    authgroup: &Option<String>,
    manual_routes: &Vec<Ipv4Net>,
    protocol: OpenConnectProtocol,
) -> NSpawn {
    let mut vpn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/openconnect"),
        name_prefix: "oc".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    do_veth(
        &mut vpn,
        veth_ip_1,
        veth_ip_2,
        veth_prefixlen,
        vpn_network,
        veth_interface,
    );

    do_firewall(&mut vpn, &veth_interface, "tun0");

    let mut openconnect_cmd = String::new();
    if let Some(servercert) = servercert {
        openconnect_cmd += &format!(" --servercert {}", servercert);
    }

    if let Some(authgroup) = authgroup {
        openconnect_cmd += &format!(" --authgroup {}", authgroup);
    }

    match protocol {
        OpenConnectProtocol::AnyConnect => {}
        OpenConnectProtocol::NC => openconnect_cmd += " --protocol=nc",
        OpenConnectProtocol::GP => openconnect_cmd += " --protocol=gp",
        OpenConnectProtocol::Pulse => openconnect_cmd += "--protocol=pulse",
    }

    let openconnect_service = format!(
        r#"[Unit]
Description=OpenConnect client
After=network.target
[Service]
ExecStart=/bin/bash -c "exec </etc/openconnect_passwd openconnect {} --non-inter --user {} {} --passwd-on-stdin"
ExecStartPre=+/sbin/iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
ExecStopPost=+/sbin/iptables -t nat -D POSTROUTING -o tun0 -j MASQUERADE
Restart=always
RestartSec=5
SyslogIdentifier=openconnect

CapabilityBoundingSet=CAP_NET_ADMIN
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=true
ProtectControlGroups=true
# PrivateDevices=true # breaks, needs tun device

SystemCallArchitectures=native
# TODO: replace with whitelist (based on @system-service)
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @module @mount @obsolete @raw-io @reboot @swap

NoNewPrivileges=yes
ProtectKernelModules=yes
ProtectKernelTunables=yes
RestrictAddressFamilies=AF_UNIX AF_INET AF_INET6 AF_NETLINK
RestrictRealtime=yes
RestrictNamespaces=yes
MemoryDenyWriteExecute=yes
[Install]
WantedBy=multi-user.target
"#,
        endpoint, username, openconnect_cmd
    );
    vpn.write_file(
        &PathBuf::from("etc/systemd/system/openconnect.service"),
        &openconnect_service,
    );
    vpn.write_file(&PathBuf::from("etc/openconnect_passwd"), password);

    vpn.exec(&["systemctl", "enable", "openconnect"]).unwrap();
    vpn.exec(&["chmod", "go-rwx", "/etc/openconnect_passwd"])
        .unwrap();

    // manual route configuration
    // this feels like a hack but it works. We have systemd-networkd
    // add the actual route because it knows exactly when the tun device
    // comes up, and can add the route then.
    let mut manual_route_string = String::new();
    for route in manual_routes {
        manual_route_string += &format!(
            r#"
[Route]
Destination={}
Scope=link
"#,
            route
        );
    }

    let tun0 = format!(
        r#"[Match]
Name=tun0

{}"#,
        manual_route_string
    );

    vpn.write_file(&PathBuf::from("etc/systemd/network/tun0.network"), &tun0);

    vpn.boot();

    return vpn;
}

pub fn create_tinc(
    veth_ip_1: Ipv4Addr,
    veth_ip_2: Ipv4Addr,
    veth_network: Ipv4Net,
    vpn_network: Ipv4Net,
    listen_port: u16,
    user_public_key: Rsa<Public>,
    key: Rsa<Private>,
    veth_interface: &str,
    routes: &Vec<Ipv4Net>,
    global_config: &GlobalConfig,
) -> NSpawn {
    assert!(routes.len() >= 1);

    let pem = key.private_key_to_pem().unwrap();
    let public_pem = String::from_utf8(key.public_key_to_pem_pkcs1().unwrap()).unwrap();

    let gateway = vpn_network.hosts().next().unwrap();
    let mut nspawn = NSpawn::new(NSpawnOptions {
        template_dir: PathBuf::from("/mnt/templates/tinc"),
        name_prefix: "tinc".to_string(),
        tmp_dir: "/mnt/tmp".into(),
        btrfs_snapshot: true,
        ..Default::default()
    })
    .unwrap();

    do_iperf(&mut nspawn);

    nspawn.add_interface(veth_interface);

    nspawn.mkdir(&PathBuf::from("etc/tinc/tinc0"));

    let mut veth_routes = String::new();
    for route in routes {
        // look to the definition of firewall.service below for why
        // we put these routes into table 1
        veth_routes += &format!(
            r#"[Route]
Gateway={}
Destination={}
Table=1
"#,
            veth_ip_2, route
        );
    }

    let veth_file_1 = format!(
        r#"[Match]
Name={}
[Network]
Address={}/{}
IPForward=true
{}"#,
        &veth_interface,
        veth_ip_1,
        veth_network.prefix_len(),
        &veth_routes
    );

    nspawn.write_file(
        &PathBuf::from(format!("etc/systemd/network/{}.network", &veth_interface)),
        &veth_file_1,
    );

    let network_file = format!(
        r#"[Match]
Name=tinc0
[Network]
Address={}/{}
IPForward=true
"#,
        gateway,
        vpn_network.prefix_len()
    );

    nspawn.write_file(
        &PathBuf::from("etc/systemd/network/tinc0.network"),
        &network_file,
    );

    // TODO: find size and use appropriate half-ish
    let first_address = vpn_network.hosts().nth(10).unwrap();
    let last_address = vpn_network.hosts().nth(20).unwrap();

    let mut dnsmasq_routes = String::new();
    for route in routes {
        dnsmasq_routes += &format!(",{},{}", route, gateway);
    }
    for route in &global_config.local_routes {
        dnsmasq_routes += &format!(",{},{}", route, gateway);
    }

    let mut default_dns = String::new();
    for dns in &global_config.default_dns {
        default_dns += &format!("server={}\n", dns);
    }

    let mut local_dns = String::new();
    for dns in &global_config.local_dns {
        // we need the rebind-domain-ok here because we have stop-dns-rebind below
        local_dns += &format!(
            "server=/{}/{}\nrebind-domain-ok=/{}/\n",
            dns.domain, dns.server, dns.domain
        );
    }

    let dnsmasq_file = format!(
        r#"bind-dynamic
interface=tinc0
except-interface=lo

dhcp-range={},{},2m

# disable some default options
dhcp-option=option:router # don't push default route
dhcp-option=option:ntp-server

no-resolv
no-hosts

{}
{}
clear-on-reload

# insert routes
dhcp-option=option:classless-static-route,{},{}{}

# some debug addresses
host-record=gate.test,{}
host-record=v1.test,{}
host-record=v2.test,{}

# don't pass along (reserved)
local=/example/
local=/invalid/
local=/local/
address=/localhost/127.0.0.1
address=/localhost/::1
local=/onion/
local=/test/
domain-needed

# some security measures (do they break anything?)
bogus-priv
dnssec
filterwin2k
stop-dns-rebind

# stop forcing Windows to timeout for DHCP
dhcp-authoritative
# since we control the entire tinc network, this is unnecessary
# and should speed things up a little bit
no-ping
"#,
        first_address,
        last_address,
        default_dns,
        local_dns,
        veth_network,
        gateway,
        dnsmasq_routes,
        gateway,
        veth_ip_1,
        veth_ip_2
    );

    nspawn.write_file(&PathBuf::from("etc/dnsmasq.conf"), &dnsmasq_file);

    let mut leak_firewall = String::new();
    for route in &global_config.local_routes {
        leak_firewall += &format!(
            "ExecStart=/sbin/iptables -A FORWARD -i tinc0 -o host0 -d {} -j ACCEPT\n",
            route,
        );
    }

    let firewall_service = format!(
        r#"[Unit]
Description=persistent firewall
DefaultDependencies=no

Before=network-pre.target
Wants=network-pre.target

Wants=systemd-modules-load.service local-fs.target
After=systemd-modules-load.service local-fs.target

Conflicts=shutdown.target
Before=shutdown.target

[Service]
Type=oneshot
RemainAfterExit=yes

# we need to NAT out of host0 if we are to allow local routes
ExecStart=/sbin/iptables -t nat -A POSTROUTING -o host0 -j MASQUERADE

# by default don't forward anything
ExecStart=/sbin/iptables -P FORWARD DROP

# allow existing connections, responses to existing connections, etc.
# this can probably be improved (security-wise), but this will work
ExecStart=/sbin/iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

# allow the VPN to work
ExecStart=/sbin/iptables -A FORWARD -i tinc0 -o {} -j ACCEPT

# only allow subnets that have been explicitly configured to be accessible locally
# this prevents someone from just adding routes to access things they shouldn't
{}

# we add the needed routes into table 1 so that a client can connect from
# 192.168.1.0/24 while the VPN is pushing 192.168.1.0/24
# i.e., we exclude local traffic (for this container) from the VPN pushed routes
ExecStart=/sbin/ip rule add iif tinc0 table 1
[Install]
WantedBy=multi-user.target
"#,
        veth_interface, leak_firewall
    );

    nspawn.write_file(
        &PathBuf::from("etc/systemd/system/firewall.service"),
        &firewall_service,
    );
    nspawn
        .exec(&["systemctl", "enable", "firewall.service"])
        .unwrap();

    nspawn.write_file(
        &PathBuf::from("etc/tinc/tinc0/rsa_key.priv"),
        &String::from_utf8(pem).unwrap(),
    );

    nspawn.write_file(
        &PathBuf::from("etc/tinc/tinc0/tinc.conf"),
        r#"Name = host
Mode = switch
ReplayWindow = 32
"#,
    );

    nspawn.mkdir(&PathBuf::from("etc/tinc/tinc0/hosts"));
    nspawn.write_file(&PathBuf::from("etc/tinc/tinc0/hosts/host"), &public_pem);

    let user_public_key = String::from_utf8(user_public_key.public_key_to_pem().unwrap()).unwrap();

    nspawn.write_file(
        &PathBuf::from("etc/tinc/tinc0/hosts/user"),
        &user_public_key,
    );

    nspawn
        .exec(&["systemctl", "enable", "systemd-networkd"])
        .unwrap();
    nspawn.exec(&["systemctl", "enable", "tinc@tinc0"]).unwrap();
    nspawn
        .exec(&["chmod", "go-rwx", "/etc/tinc/tinc0/rsa_key.priv"])
        .unwrap();

    nspawn.set_port_forward(listen_port, 655);
    nspawn.boot();

    return nspawn;
}

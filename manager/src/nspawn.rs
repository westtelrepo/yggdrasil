use nix::sys::signal::kill;
use nix::sys::signal::Signal;
use nix::sys::stat::fchmodat;
use nix::sys::stat::FchmodatFlags;
use nix::sys::stat::Mode;
use nix::unistd::Pid;
use std::ffi::{OsStr, OsString};
use std::fs::{create_dir, remove_file, File};
use std::io;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::process;
use std::process::Command;
use std::process::Stdio;
use tempfile;
use tempfile::TempDir;
use uuid::Uuid;

pub struct NSpawnOptions {
    pub template_dir: PathBuf,
    pub name_prefix: String,
    pub btrfs_snapshot: bool,
    pub tmp_dir: PathBuf,
}

impl Default for NSpawnOptions {
    fn default() -> Self {
        NSpawnOptions {
            template_dir: PathBuf::from("/home/vagrant/buster_default"), // TODO
            name_prefix: "temp".to_string(),
            btrfs_snapshot: false,
            tmp_dir: PathBuf::from("/tmp"),
        }
    }
}

pub struct NSpawn {
    child: Option<process::Child>,
    tempdir: TempDir,
    name: String,
    host_port: Option<u16>,
    container_port: Option<u16>,
    network_interfaces: Vec<String>,
    allow_ppp: bool,
    btrfs: bool,
    uuid: Uuid,
}

impl NSpawn {
    pub fn new(options: NSpawnOptions) -> Option<Self> {
        let tempdir =
            tempfile::tempdir_in(options.tmp_dir).expect("could not create temporary directory");

        // make it so temporary directory can only be read/written/execed by us (root)
        // it would be better to set a mode in mkdir, but this should be as secure
        // there is a period of time where after the directory is created it can
        // be read, but there is nothing in it during this period
        // making the point kinda moot. Also it can't be written to by others (by default)
        fchmodat(
            None,
            tempdir.path(),
            Mode::S_IRWXU,
            FchmodatFlags::FollowSymlink,
        )
        .expect("could not change mode");

        let mut container_path = tempdir.path().to_path_buf();
        container_path.push("rootfs");

        let result = if !options.btrfs_snapshot {
            Command::new("cp")
                .arg("-a")
                .arg("-T")
                .arg(options.template_dir)
                .arg(container_path)
                .output()
                .expect("could not copy")
        } else {
            Command::new("btrfs")
                .arg("subvolume")
                .arg("snapshot")
                .arg(options.template_dir)
                .arg(container_path)
                .output()
                .expect("could not snapshot")
        };

        assert!(result.status.success());

        let uuid = Uuid::new_v4();

        let mut ret = NSpawn {
            child: None,
            tempdir,
            name: options.name_prefix + "-" + &uuid.to_simple().to_string(),
            host_port: None,
            container_port: None,
            network_interfaces: Vec::new(),
            allow_ppp: false,
            btrfs: options.btrfs_snapshot,
            uuid,
        };

        ret.rm(&PathBuf::from("etc/hostname")).unwrap();

        // delete possibly existing machine-id files so we can override them with
        // our own self.uuid
        ret.rm(&PathBuf::from("etc/machine-id")).unwrap();
        ret.rm(&PathBuf::from("var/lib/dbus/machine-id")).unwrap();

        Some(ret)
    }

    // we disable user namespacing as well with this
    // TODO: find out how not to need to do that
    // also you must call this before boot()
    pub fn allow_ppp(&mut self) {
        self.allow_ppp = true;
    }

    pub fn add_interface(&mut self, interface: &str) {
        self.network_interfaces.push(interface.to_string());
    }

    pub fn set_port_forward(&mut self, host: u16, container: u16) {
        self.host_port = Some(host);
        self.container_port = Some(container);
    }

    // DO NOT use an absolute path for filename!
    pub fn write_file(&mut self, filename: &Path, file_contents: &str) {
        assert!(!filename.is_absolute());
        let mut path = self.tempdir.path().to_path_buf();
        path.push("rootfs");
        path.push(&filename);

        let mut file = File::create(path).unwrap();
        file.write_all(file_contents.as_bytes()).unwrap();
    }

    // DO NOT use an absolute path for filename!
    pub fn mkdir(&mut self, filename: &Path) {
        assert!(!filename.is_absolute());
        let mut path = self.tempdir.path().to_path_buf();
        path.push("rootfs");
        path.push(&filename);

        create_dir(&path).unwrap();
    }

    // DO NOT use an absolute path for filename!
    pub fn rm(&mut self, filename: &Path) -> io::Result<()> {
        assert!(!filename.is_absolute());
        let mut path = self.directory();
        path.push(&filename);

        match remove_file(&path) {
            Ok(()) => Ok(()),
            Err(e) => {
                // ignore not found error
                if e.kind() == io::ErrorKind::NotFound {
                    Ok(())
                } else {
                    Err(e)
                }
            }
        }
    }

    pub fn exec<I, S>(&mut self, args: I) -> io::Result<process::Output>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        let mut command = Command::new("systemd-nspawn");

        let child = command
            .arg("-D")
            .arg(self.directory())
            .args(&["--settings=no", "--as-pid2", "-q"])
            .arg("--machine")
            .arg(&self.name)
            // don't use -U with exec, this allows write_file/mkdir/etc to be used after exec
            .args(&["--private-users=0", "--private-users-chown"])
            .arg("--uuid")
            .arg(&self.uuid.to_simple().to_string());

        child.args(args).output()
    }

    pub fn boot(&mut self) {
        let mut command = Command::new("systemd-nspawn");

        let child = command
            .arg("-D")
            .arg(self.directory())
            .args(&["--settings=no", "-q", "-b"])
            .arg("--network-bridge=yggdrasil")
            .arg("--machine")
            .arg(&self.name)
            .arg("--uuid")
            .arg(&self.uuid.to_simple().to_string());

        let child = if self.allow_ppp {
            child.arg("--bind=/dev/ppp")
        } else {
            child.arg("-U")
        };

        let child = if let (Some(host_port), Some(container_port)) =
            (self.host_port, self.container_port)
        {
            child
                .arg("--port")
                .arg(format!("tcp:{}:{}", host_port, container_port))
                .arg("--port")
                .arg(format!("udp:{}:{}", host_port, container_port))
        } else {
            child
        };

        let mut arg_vec: Vec<OsString> = Vec::new();
        for interface in &self.network_interfaces {
            arg_vec.push(OsString::from("--network-interface"));
            arg_vec.push(OsString::from(&interface));
        }

        let child = child.args(arg_vec);

        let child = child
            .stdin(Stdio::null())
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()
            .expect("could not boot container");

        self.child = Some(child);
    }

    pub fn directory(&mut self) -> PathBuf {
        let mut ret = self.tempdir.path().to_path_buf();
        ret.push("rootfs");
        ret
    }

    pub fn name(&self) -> &str {
        return &self.name;
    }
}

impl Drop for NSpawn {
    fn drop(&mut self) {
        if let Some(ref mut child) = self.child {
            kill(Pid::from_raw(child.id() as i32), Signal::SIGTERM).unwrap();
            child.wait().unwrap();
        }

        if self.btrfs {
            Command::new("btrfs")
                .arg("subvolume")
                .arg("delete")
                .arg(self.directory())
                .output()
                .expect("Could not delete snapshot");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread::sleep;
    use std::time::Duration;

    #[test]
    fn it_works() {
        let tmpdir: PathBuf;
        {
            let mut container = NSpawn::new(Default::default()).unwrap();

            tmpdir = container.directory();

            let mut os_release = tmpdir.clone();
            os_release.push("etc");
            os_release.push("os-release");
            assert!(os_release.exists());
        }
        assert!(!tmpdir.exists());
    }

    #[test]
    fn exec() {
        let mut container = NSpawn::new(Default::default()).unwrap();

        let result = container.exec(&["/bin/ls", "/etc/"]).unwrap();

        assert!(result.status.success());
        let output = String::from_utf8(result.stdout).unwrap();

        assert!(output.find("os-release").is_some());
    }

    #[test]
    fn boot() {
        let mut container = NSpawn::new(Default::default()).unwrap();

        container.boot();

        sleep(Duration::from_secs(1));
    }

    #[test]
    fn template_dir() {
        let mut tinc = NSpawn::new(NSpawnOptions {
            template_dir: "/home/vagrant/tinc_default".into(),
            ..Default::default()
        })
        .unwrap();

        let mut default = NSpawn::new(Default::default()).unwrap();

        // default doesn't have tincd
        let result = default.exec(&["which", "tincd"]).unwrap();
        assert!(!result.status.success());

        // but this template does
        let result = tinc.exec(&["which", "tincd"]).unwrap();
        assert!(result.status.success());
    }

    #[test]
    fn name() {
        let named = NSpawn::new(NSpawnOptions {
            name_prefix: "hello".to_string(),
            ..Default::default()
        })
        .unwrap();

        assert!(named.name().starts_with("hello-"));
    }

    #[test]
    fn btrfs_test() {
        let tmpdir: PathBuf;
        {
            let mut nspawn = NSpawn::new(NSpawnOptions {
                template_dir: "/mnt/templates/buster".into(),
                tmp_dir: "/mnt/tmp".into(),
                btrfs_snapshot: true,
                ..Default::default()
            })
            .unwrap();

            tmpdir = nspawn.directory();

            let result = nspawn.exec(&["/bin/ls", "/etc/"]).unwrap();

            assert!(result.status.success());
            let output = String::from_utf8(result.stdout).unwrap();

            assert!(output.find("os-release").is_some());
        }
        assert!(!tmpdir.exists());
    }
}

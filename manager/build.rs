use std::process::Command;

fn main() {
    let output = Command::new("git")
        .args(&["rev-parse", "--verify", "HEAD"])
        .output()
        .expect("could not run git");
    let git_hash = String::from_utf8(output.stdout).unwrap();
    println!("cargo:rustc-env=GIT_HASH={}", git_hash);

    let output = Command::new("git")
        .args(&["status", "--porcelain"])
        .output()
        .expect("could not run git");
    let output = String::from_utf8(output.stdout).unwrap().trim().to_owned();
    let git_dirty = if output.len() == 0 { false } else { true };
    println!("cargo:rustc-env=GIT_DIRTY={}", git_dirty);

    let output = Command::new("git")
        .args(&["rev-parse", "--verify", "--short", "HEAD"])
        .output()
        .expect("could not run git");
    let git_short_hash = String::from_utf8(output.stdout).unwrap();
    println!("cargo:rustc-env=GIT_SHORT_HASH={}", git_short_hash);
}

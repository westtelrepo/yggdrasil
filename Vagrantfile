# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/contrib-buster64"

  config.vm.define "dev", primary: true do |dev|
    dev.vm.hostname = "dev"

    dev.vm.network "forwarded_port", guest: 22, host: 4443, id: "ssh"

    dev.vm.provider "virtualbox" do |vb|
      vb.memory = 8192
      vb.cpus = 6
    end

    dev.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "dev_env.yml"
    end

    dev.vm.provision "shell", run: "always", inline: <<-SHELL
      mkdir -p /vagrant/target
      mkdir -p /home/vagrant/target
      mount --bind /home/vagrant/target /vagrant/target
      chown vagrant:vagrant /home/vagrant/target

      # needed for PPTP and FTP
      modprobe ip_nat_pptp
      modprobe nf_conntrack_ftp
      modprobe nf_nat_ftp
      sysctl -w net.netfilter.nf_conntrack_helper=1

      # needed for yarn (symlinks, etc break)
      mkdir -p /vagrant/manager_web/node_modules
      mkdir -p /home/vagrant/node_modules
      mount --bind /home/vagrant/node_modules /vagrant/manager_web/node_modules
      chown vagrant:vagrant /home/vagrant/node_modules
    SHELL

    # NOTE: this is needed for PPTP to work, VirtualBox's NAT does NOT work with PPTP
    dev.vm.network "public_network", auto_config: false

    dev.vm.network "private_network", ip: "10.247.1.10"

    dev.vm.network "private_network", ip: "10.10.10.5", virtualbox__intnet: "vpn_networks"

    dev.vm.network "forwarded_port", guest: 443, host: 8443
  end

  config.vm.define "ocserv", autostart: false do |ocserv|
    ocserv.vm.hostname = "ocserv"
    ocserv.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "ocserv.yml"
    end

    ocserv.vm.network "private_network", ip: "10.10.10.10", virtualbox__intnet: "vpn_networks"
  end

  config.vm.define "ocserv2", autostart: false do |ocserv2|
    ocserv2.vm.hostname = "ocserv2"
    ocserv2.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "ocserv2.yml"
    end

    ocserv2.vm.network "private_network", ip: "10.10.10.11", virtualbox__intnet: "vpn_networks"
  end
end

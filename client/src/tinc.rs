use std::fs;
use std::io;
use std::io::prelude::*;
use std::process;
use std::process::Command;
use tempfile;
use tempfile::TempDir;

// TODO: better way of doing this
#[cfg(target_os = "linux")]
static TINCD_PATH: &'static str = "/usr/sbin/tincd";
#[cfg(target_os = "windows")]
static TINCD_PATH: &'static str = "C:\\Program Files (x86)\\tinc\\tincd.exe";

pub struct TincOptions {
    pub name: String,
}

pub struct Tinc {
    tmpdir: TempDir,
    name: String,
    public_key: String,
    private_key: String,
    child: Option<process::Child>,
    port: Option<u16>,
    hosts: Vec<TincHostConfig>,
}

pub struct TincHostConfig {
    pub public_key: String,
    pub prefix: String, // TODO: don't do this
    pub name: String,
}

impl Tinc {
    pub fn new(options: TincOptions) -> io::Result<Tinc> {
        let mut public_key = String::new();
        let mut private_key = String::new();

        {
            let tmpdir = tempfile::tempdir()?;

            Command::new(TINCD_PATH)
                .arg("-c")
                .arg(tmpdir.path().as_os_str())
                .arg("-K3072")
                .output()?;

            let mut pub_file = tmpdir.path().to_path_buf();
            pub_file.push("rsa_key.pub");
            let mut file = fs::File::open(pub_file)?;
            file.read_to_string(&mut public_key)?;

            let mut priv_file = tmpdir.path().to_path_buf();
            priv_file.push("rsa_key.priv");
            let mut file = fs::File::open(priv_file)?;
            file.read_to_string(&mut private_key)?;
        }

        let tmpdir = tempfile::tempdir()?;

        let mut hosts_dir = tmpdir.path().to_path_buf();
        hosts_dir.push("hosts");
        fs::create_dir(hosts_dir)?;

        return Ok(Tinc {
            tmpdir,
            name: options.name,
            public_key,
            private_key,
            child: None,
            port: None,
            hosts: Vec::new(),
        });
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn start(&mut self) -> io::Result<()> {
        self.write_files()?;

        let mut pid_file = self.tmpdir.path().to_path_buf();
        pid_file.push("tincd.pid");

        self.child = Some(
            Command::new(TINCD_PATH)
                .arg("-c")
                .arg(self.tmpdir.path())
                .arg("--debug=2")
                .arg("--no-detach")
                .arg("--pidfile")
                .arg(pid_file)
                .spawn()?,
        );
        Ok(())
    }

    pub fn set_port(&mut self, port: u16) {
        self.port = Some(port);
    }

    fn write_files(&mut self) -> io::Result<()> {
        let mut tinc_conf = self.tmpdir.path().to_path_buf();
        tinc_conf.push("tinc.conf");
        let mut tinc = fs::File::create(tinc_conf)?;
        writeln!(&mut tinc, "Name = {}", &self.name)?;
        if let Some(port) = self.port {
            writeln!(&mut tinc, "Port = {}", port)?;
        }
        writeln!(&mut tinc, "ConnectTo = {}", "host")?; // TODO: not this
        writeln!(&mut tinc, "Mode = switch")?;

        writeln!(&mut tinc, "MaxTimeout = 15")?;
        writeln!(&mut tinc, "PingInterval = 15")?;
        writeln!(&mut tinc, "ReplayWindow = 32")?;

        let mut priv_key_file = self.tmpdir.path().to_path_buf();
        priv_key_file.push("rsa_key.priv");
        let mut priv_key = fs::File::create(priv_key_file)?;
        write!(priv_key, "{}", self.private_key)?;

        let mut host_dir = self.tmpdir.path().to_path_buf();
        host_dir.push("hosts");

        for host in &self.hosts {
            let mut filename = host_dir.clone();
            filename.push(&host.name);
            let mut file = fs::File::create(filename)?;
            writeln!(file, "{}", host.prefix)?;
            writeln!(file, "{}", host.public_key)?;
        }

        Ok(())
    }

    pub fn add_host(&mut self, config: TincHostConfig) {
        self.hosts.push(config);
    }

    pub fn public_key(&self) -> &str {
        &self.public_key
    }
}

impl Drop for Tinc {
    fn drop(&mut self) {
        if let Some(ref mut child) = self.child {
            child.kill().unwrap();
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn it_works() {
        let tmpdir: PathBuf;
        {
            let options = TincOptions {
                name: "some_name".to_string(),
            };
            let tinc = Tinc::new(options).unwrap();

            assert!(tinc.name().len() > 0);

            tmpdir = tinc.tmpdir.path().to_path_buf();
        }

        // the temporary directory it creates should be gone when it exits
        assert!(!tmpdir.exists());
    }
}

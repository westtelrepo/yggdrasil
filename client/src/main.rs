#![forbid(unsafe_code)]
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate hostname;
extern crate jsonwebtoken;
extern crate serde;
extern crate tempfile;
extern crate uuid;

mod tinc;

use jsonwebtoken::dangerous_insecure_decode;
use reqwest::{blocking::Client, Url};
use std::env;
use std::io;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;
use tinc::{Tinc, TincHostConfig, TincOptions};

fn main() {
    let base_url = Url::parse(match option_env!("BASE_URL") {
        None => "https://yggdrasil.test.westtel.com/",
        Some(url) => url,
    })
    .unwrap();

    println!("Connecting to {}", base_url);

    let args: Vec<String> = env::args().collect();

    let rclient = Client::builder()
        .timeout(Duration::from_secs(70))
        .build()
        .unwrap();

    let options = TincOptions {
        name: "user".to_string(),
    };
    let mut tinc = Tinc::new(options).unwrap();

    #[derive(Serialize)]
    #[serde(tag = "type")]
    enum VpnType {
        Tinc { public_key: String },
    }

    #[derive(Serialize)]
    struct Create {
        site: String,
        vpn: VpnType,
        hostname: Option<String>,
    }

    let mut site = String::new();

    if args.len() == 3 && args[1] == "--url" {
        site = args[2].split(":").nth(1).unwrap().trim().to_owned();
    } else {
        println!("Enter token");

        let stdin = io::stdin();
        stdin.read_line(&mut site).unwrap();
        site = site.trim().to_string();
    }

    #[derive(Deserialize)]
    struct Claims {
        sub: String, // vpn id
    }

    let claims = dangerous_insecure_decode::<Claims>(&site).unwrap();

    let create = Create {
        site: claims.claims.sub,
        vpn: VpnType::Tinc {
            public_key: tinc.public_key().to_string(),
        },
        hostname: hostname::get()
            .ok()
            .map(|x| x.to_string_lossy().to_string()),
    };

    let response = rclient
        .post(base_url.join("connection").unwrap())
        .bearer_auth(&site)
        .json(&create)
        .send()
        .unwrap();

    #[derive(Deserialize)]
    struct HostConfig {
        address: std::net::SocketAddr,
        public_key: String,
        name: String,
    }

    #[derive(Deserialize)]
    struct Response {
        id: String,
        tinc_hosts: Vec<HostConfig>,
    }

    let param: Response = response.json().unwrap();

    let client = {
        TincHostConfig {
            name: "user".to_string(),
            public_key: tinc.public_key().to_string(),
            prefix: "".to_string(),
        }
    };

    tinc.add_host(client);

    for host in param.tinc_hosts {
        let host = TincHostConfig {
            name: host.name,
            public_key: host.public_key,
            prefix: format!("Address = {} {}", host.address.ip(), host.address.port()),
        };

        tinc.add_host(host);
    }

    tinc.start().unwrap();

    let to_check = base_url
        .join("connection/")
        .unwrap()
        .join(&format!("{}/", &param.id)) // we need to put a slash at the end, otherwise it will be snapped off
        .unwrap()
        .join("still_exists")
        .unwrap();

    loop {
        let result = rclient.get(to_check.clone()).send();
        let result = match result {
            Ok(result) => result.error_for_status(),
            Err(e) => Err(e),
        };
        match result {
            Ok(result) => {
                let result: bool = result.json().unwrap();
                // if the endpoint returns false, then the connection has been stopped
                if !result {
                    break;
                }
            }
            Err(err) => {
                println!("connection error: {}", err);
                // sleep after a connection error so we don't pummel things
                sleep(Duration::from_secs(5));
            }
        };
    }

    if cfg!(target_os = "windows") {
        // otherwise routes have a tendency to stay around longer than they should for some reason
        match Command::new("ipconfig")
            .args(&["/release", "Tinc0"])
            .output()
        {
            Ok(_) => {}
            Err(e) => println!("Could not release DHCP: {}", e),
        }
    }

    println!("tearing down connection");
}

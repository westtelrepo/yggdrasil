import React from 'react';
import ReactDOM from 'react-dom';
import { css } from 'emotion';

class App extends React.Component {
    constructor() {
        super();
        this.state = { sites: [], connections: [] };
    }

    render() {
        const buttonStyle = css`width: 120px; font-size: inherit`;
        const divStyle = css`font-size: 20px`;

        const sort_name = (a, b) => { if (a.name < b.name) return -1; else if (a.name > b.name) return 1; else return 0; };

        const { sites, connections } = this.state;
        return <div className={divStyle}>
            { connections.map(x =>
                <div key={x.id}>
                    {x.for_user} { x.hostname ? `on ${x.hostname}` : '' }: {x.vpn_id} ({x.id})<br />
                    <div className={css`padding-left: 20px`}>
                        <button className={css`font-size: 15px`}
                            onClick={this.deleteConnection.bind(this, x.id)}>Disconnect
                        </button>
                        <span className={css`padding-left: 8px`}>{ sites.find(y => y.id === x.vpn_id).name }</span>
                    </div>
                    { x.routes.map(r => <div className={css`padding-left: 20px`} key={r}>{r}</div>) }
                </div>) }
            <br />
            <br />
            <table><tbody>
            { sites.sort(sort_name).map(x => <tr key={x.id}>
                <td><button className={buttonStyle} onClick={this.promptSite.bind(this, x.id)}>{x.id}</button></td>
                <td>{x.name}</td>
            </tr>) }
            </tbody></table>
        </div>;
    }

    componentDidMount() {
        fetch('/site').then(result => result.json()).then(items => this.setState({sites: items}));
        setInterval(() => {
            fetch('/connection').then(result => result.json()).then(connections => this.setState({connections}));
        }, 3000);
    }

    promptSite(id) {
        fetch(`/site/${id}`).then(result => result.text()).then(text => window.location = "x-yggdrasil:" + text);
    }

    deleteConnection(id) {
        fetch(`/connection/${id}`, {method: 'DELETE'});
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);

#!/bin/bash

mkdir -p lib

if [[ "$NODE_ENV" == "production" ]] ; then
    browserify src/index.js -t [ babelify ] -g [ envify --NODE_ENV $NODE_ENV ] -g uglifyify | uglifyjs --compress --mangle > lib/index.js
else
    browserify src/index.js -t [ babelify ] > lib/index.js
fi
